﻿using System;
using Xunit;
using Assignment_1_RPG.Classes;

namespace Assignment_1_RPG_Tests
{
    public class ItemsAndEquipmentTests
    {
        Weapon testAxe = new()
        {
            name = "Common axe",
            slot = EquipmentSlot.SLOT_WEAPON,
            levelRequirement = 1,
            category = WeaponType.WPN_AXE,
            baseDamage = 7,
            attackSpeed = 1.1f
        };

        Armor testPlateBody = new()
        {
            name = "Common plate body armor",
            slot = EquipmentSlot.SLOT_BODY,
            levelRequirement = 1,
            material = Material.ARMOR_PLATE,
            attributes = new PrimaryAttributes(1, 0, 0, 2)
        };

        Weapon testBow = new()
        {
            name = "Common bow",
            levelRequirement = 1,
            slot = EquipmentSlot.SLOT_WEAPON,
            category = WeaponType.WPN_BOW,
            baseDamage = 12,
            attackSpeed = 0.8f
        };

        Armor testClothHead = new()
        {
            name = "common cloth head armor",
            levelRequirement = 1,
            slot = EquipmentSlot.SLOT_HEAD,
            material = Material.ARMOR_CLOTH,
            attributes = new PrimaryAttributes(0, 0, 5, 1)
        };

        [Fact]
        public void Equip_TryHighLevelWeapon_ShouldThrowInvalidWeaponException()
        {
            //Arrange
            Warrior testWarrior = new("Thor");
            testAxe.levelRequirement = 2;
            //Act & Assert
            Assert.Throws<InvalidWeaponException>(() => testWarrior.Equip(testAxe));
        }
        [Fact]
        public void Equip_TryHighLevelArmor_ShouldThrowInvalidArmorException()
        {
            //Arrange
            Warrior testWarrior = new("Thor");
            testPlateBody.levelRequirement = 2;
            //Act & Assert
            Assert.Throws<InvalidArmorException>(() => testWarrior.Equip(testPlateBody));
        }
        [Fact]
        public void Equip_TryWrongWeaponType_ShouldThrowInvalidWeaponException()
        {
            //Arrange
            Warrior testWarrior = new("Thor");
            //Act & Assert
            Assert.Throws<InvalidWeaponException>(() => testWarrior.Equip(testBow));
        }
        [Fact]
        public void Equip_TryWrongArmorType_ShouldThrowInvalidArmorException()
        {
            //Arrange
            Warrior testWarrior = new("Thor");
            //Act & Assert
            Assert.Throws<InvalidArmorException>(() => testWarrior.Equip(testClothHead));
        }
        [Fact]
        public void Equip_TryValidWeapon_ShouldReturnString()
        {
            //Arrange
            Warrior testWarrior = new("Thor");
            string expected = "New weapon equipped!";
            testAxe.levelRequirement = 1;
            //Act & Assert
            Assert.Equal(expected, testWarrior.Equip(testAxe));
        }
        [Fact]
        public void Equip_TryValidArmor_ShouldReturnString()
        {
            //Arrange
            Warrior testWarrior = new("Thor");
            string expected = "New armor equipped!";
            testPlateBody.levelRequirement = 1;
            //Act & Assert
            Assert.Equal(expected, testWarrior.Equip(testPlateBody));
        }
        [Fact]
        public void CalculateDPS_NoWeaponEquipped_ShouldReturnFloat()
        {
            //Arrange
            Warrior testWarrior = new("Thor");
            float expected = 1 * (1 + (5 / 100));
            //Act & Assert
            Assert.Equal(expected, testWarrior.CalculateDPS());
        }
        [Fact]
        public void CalculateDPS_WithWeaponEquipped_ShouldReturnFloat()
        {
            //Arrange
            Warrior testWarrior = new("Thor");
            float expected = (7 * 1.1f)*(1 + (5 / 100));
            testAxe.levelRequirement = 1;
            testWarrior.Equip(testAxe);
            //Act & Assert
            Assert.Equal(expected, testWarrior.CalculateDPS());
        }
        [Fact]
        public void CalculateDPS_WithWeaponAndArmorEquipped_ShouldReturnFloat()
        {
            //Arrange
            Warrior testWarrior = new("Thor");
            float expected = (7 * 1.1f) * (1 + ((5 + 1) / 100));
            testAxe.levelRequirement = 1;
            testPlateBody.levelRequirement = 1;
            testWarrior.Equip(testAxe);
            testWarrior.Equip(testPlateBody);
            //Act & Assert
            Assert.Equal(expected, testWarrior.CalculateDPS());
        }
    }
}