using System;
using Xunit;
using Assignment_1_RPG.Classes;

namespace Assignment_1_RPG_Tests
{
    public class CharacterTests
    {
        //Leveling Tests
        [Fact]
        public void Hero_CheckStartLevel_ShouldReturnOne()
        {
            //Arrange & Act
            Hero input = new Ranger("James");
            //Assert
            Assert.Equal(1, input.Level);
        }
        [Fact]
        public void LevelUp_LevelUpOnce_ShouldReturnTwo()
        {
            //Arrange
            Ranger input = new("James");
            //Act
            input.LevelUp(1);
            //Assert
            Assert.Equal(2, input.Level);
        }
        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void LevelUp_TryInvalidAmount_ShouldThrowArgumentException(int amount)
        {
            //Arrange
            Hero input = new Ranger("James");
            //Act & Assert
            Assert.Throws<ArgumentException>(() => input.LevelUp(amount));
        }
        //Default Attributes Tests
        [Fact]
        public void Mage_CorrectDefaultAttributes_ShouldReturnAttributes()
        {
            //Arrange
            Mage input = new Mage("Chris");
            PrimaryAttributes expected = new(1, 1, 8, 5);
            //Act & Assert
            Assert.Equal(expected, input.basePrimaryAttributes);
        }
        [Fact]
        public void Ranger_CorrectDefaultAttributes_ShouldReturnAttributes()
        {
            //Arrange
            Ranger input = new("James");
            PrimaryAttributes expected = new(1, 7, 1, 8);
            //Act & Assert
            Assert.Equal(expected, input.basePrimaryAttributes);
        }
        [Fact]
        public void Rogue_CorrectDefaultAttributes_ShouldReturnAttributes()
        {
            //Arrange
            Rogue input = new("Thor");
            PrimaryAttributes expected = new(2, 6, 1, 8);
            //Act & Assert
            Assert.Equal(expected, input.basePrimaryAttributes);
        }
        [Fact]
        public void Warrior_CorrectDefaultAttributes_ShouldReturnAttributes()
        {
            //Arrange
            Warrior input = new("Kevin");
            PrimaryAttributes expected = new(5, 2, 1, 10);
            //Act & Assert
            Assert.Equal(expected, input.basePrimaryAttributes);
        }
        //Leveling Up Attributes Test
        [Fact]
        public void Mage_LevelUpAttributes_ShouldReturnAttributes()
        {
            //Arrange
            Mage input = new("Chris");
            PrimaryAttributes expected = new(2, 2, 13, 8);
            //Act
            input.LevelUp(1);
            //Assert
            Assert.Equal(expected, input.basePrimaryAttributes);
        }
        [Fact]
        public void Ranger_LevelUpAttributes_ShouldReturnAttributes()
        {
            //Arrange
            Ranger input = new("Chris");
            PrimaryAttributes expected = new(2, 12, 2, 10);
            //Act
            input.LevelUp(1);
            //Assert
            Assert.Equal(expected, input.basePrimaryAttributes);
        }
        [Fact]
        public void Rogue_LevelUpAttributes_ShouldReturnAttributes()
        {
            //Arrange
            Rogue input = new("Chris");
            PrimaryAttributes expected = new(3, 10, 2, 11);
            //Act
            input.LevelUp(1);
            //Assert
            Assert.Equal(expected, input.basePrimaryAttributes);
        }
        [Fact]
        public void Warrior_LevelUpAttributes_ShouldReturnAttributes()
        {
            //Arrange
            Warrior input = new("Chris");
            PrimaryAttributes expected = new(8, 4, 2, 15);
            //Act
            input.LevelUp(1);
            //Assert
            Assert.Equal(expected, input.basePrimaryAttributes);
        }
        //Secondary stats on level up
        [Fact]
        public void Warrior_SecondaryAttributesOnLevelUp_ShouldReturnAttributes()
        {
            //Arrange
            Warrior input = new("Chris");
            int[] expected = new int[] { 150, 12, 2 };
            SecondaryAttributes actual;
            //Act
            input.LevelUp(1);
            actual = input.secondaryAttributes;
            //Assert
            Assert.Equal(expected, new int[] { actual.Health(), actual.ArmorRating(), actual.ElementalResistance()});
        }
    }
}
