﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1_RPG.Classes
{
    public enum EquipmentSlot
    {
        SLOT_HEAD,
        SLOT_BODY,
        SLOT_LEGS,
        SLOT_WEAPON
    }
    public enum WeaponType
    {
        WPN_AXE,
        WPN_BOW,
        WPN_DAGGER,
        WPN_HAMMER,
        WPN_STAFF,
        WPN_SWORD,
        WPN_WAND
    }

    public enum Material
    {
        ARMOR_CLOTH,
        ARMOR_LEATHER,
        ARMOR_MAIL,
        ARMOR_PLATE
    }

    /// <summary>
    /// Exists as data storage (could be replaced by a file or database
    /// </summary>
    /// 
    public static class Gamerules
    {
        public static Random rand = new Random();

        public static Dictionary<string, PrimaryAttributes> HeroLevelGains = new Dictionary<string, PrimaryAttributes>()
        {
            {"Mage", new PrimaryAttributes(1, 1, 5, 3)},
            {"Ranger", new PrimaryAttributes(1, 5, 1, 2)},
            {"Rogue", new PrimaryAttributes(1, 4, 1, 3)},
            {"Warrior", new PrimaryAttributes(3, 2, 1, 5)}
        };

        public static Dictionary<string, List<Material>> HeroSupportedMaterials = new Dictionary<string, List<Material>>()
        {
            {"Mage", new List<Material>(){Material.ARMOR_CLOTH} },
            {"Ranger", new List<Material>(){Material.ARMOR_LEATHER, Material.ARMOR_MAIL} },
            {"Rogue", new List<Material>(){Material.ARMOR_LEATHER, Material.ARMOR_MAIL} },
            {"Warrior", new List<Material>(){Material.ARMOR_MAIL, Material.ARMOR_PLATE} }
        };

        public static Dictionary<string, List<WeaponType>> HeroSupportedWeapons = new Dictionary<string, List<WeaponType>>()
        {
            {"Mage", new List<WeaponType>(){WeaponType.WPN_STAFF, WeaponType.WPN_WAND} },
            {"Ranger", new List<WeaponType>(){WeaponType.WPN_BOW} },
            {"Rogue", new List<WeaponType>(){WeaponType.WPN_DAGGER, WeaponType.WPN_SWORD} },
            {"Warrior", new List<WeaponType>(){WeaponType.WPN_AXE, WeaponType.WPN_HAMMER, WeaponType.WPN_SWORD} }
        };

        public static Dictionary<string, Weapon> StarterWeapons = new Dictionary<string, Weapon>()
        {
            {"Mage", new Weapon("old Wooden Staff", EquipmentSlot.SLOT_WEAPON, 1, WeaponType.WPN_STAFF, 3, 1.2f) },
            {"Ranger", new Weapon("old wooden bow", EquipmentSlot.SLOT_WEAPON, 1, WeaponType.WPN_BOW, 4, 1.2f) },
            {"Rogue", new Weapon("rusty dagger", EquipmentSlot.SLOT_WEAPON, 1, WeaponType.WPN_DAGGER, 2, 2f) },
            {"Warrior", new Weapon("rusty axe", EquipmentSlot.SLOT_WEAPON, 1, WeaponType.WPN_AXE, 6, 1.0f) }
        };

        public static Dictionary<WeaponType, string> weaponTypeWeaponName = new Dictionary<WeaponType, string>()
        {
            {WeaponType.WPN_AXE, "Axe" },
            {WeaponType.WPN_BOW, "Bow" },
            {WeaponType.WPN_DAGGER, "Dagger" },
            {WeaponType.WPN_HAMMER, "Hammer" },
            {WeaponType.WPN_STAFF, "Staff" },
            {WeaponType.WPN_SWORD, "Sword" },
            {WeaponType.WPN_WAND, "Wand" }
        };

        public static Dictionary<EquipmentSlot, string> EquipmentSlotArmorName = new Dictionary<EquipmentSlot, string>()
        {
            {EquipmentSlot.SLOT_HEAD, "Headwear" },
            {EquipmentSlot.SLOT_BODY, "Chestpiece" },
            {EquipmentSlot.SLOT_LEGS, "Shoes" }
        };

        public static Dictionary<Material, string> MaterialToString = new Dictionary<Material, string>()
        {
            {Material.ARMOR_CLOTH, "Cloth" },
            {Material.ARMOR_LEATHER, "Leather" },   
            {Material.ARMOR_MAIL, "Mail" },
            {Material.ARMOR_PLATE, "Plate" }
        };

        public static List<string> Locations = new List<string>()
        {
            "River of Grace",
            "Pit of Darkness",
            "The Parking Garage Near Noroff",
            "The Moon",
            "The Lost Woods",
            "Goblin Cave",
            "Mount Ablinicloblina",
            "Tilted Towers",
            "Burning Forest"
        };

        public static List<string> WeaponMaterialPrefixes = new List<string>
        {
            "Diamond",
            "Iron",
            "Golden",
            "Copper",
            "Wooden",
            "Plastic",
            "Styrofoam",
             "Balloon",
            "Titanium",
            "Cardboard"
        };

        public static List<string> AdjectivePrefixes = new List<string>
        {
            "Rusty",
            "Dusty",
            "Glow-in-the-Dark",
            "Shiny",
            "Hardened",
            "Gold-coated",
            "Enchanted",
            "Quirky",
            "Scuffed",
            "Fancy",
            "Stinky",
            "Legendary",
            "Kinda boring",
            "Spiritual"
        };

        public static List<string> EnemyNames = new List<string>
        {
            "Goblin",
            "Giant",
            "Gamer",
            "Demigod",
            "Elf",
            "Dragon",
            "Alghoul",
            "Drowner",
            "Snorlax",
            "Griffin",
            "Troll",
            "Spider"
        };

        /// <summary>
        /// Generates a random weapon based on a hero
        /// </summary>
        /// <param name="h">ero to base weapon on</param>
        /// <returns>random weapon</returns>
        public static Weapon GenerateRandomWeapon(Hero h)
        {
            Weapon wpn;
            Array wTarr = Enum.GetValues(typeof(WeaponType));
            WeaponType randomWeaponType = (WeaponType)wTarr.GetValue(rand.Next(wTarr.Length));

            StringBuilder weaponName = new StringBuilder();
            weaponName.Append(Gamerules.AdjectivePrefixes[rand.Next(Gamerules.AdjectivePrefixes.Count)] + " ");
            weaponName.Append(Gamerules.WeaponMaterialPrefixes[rand.Next(Gamerules.WeaponMaterialPrefixes.Count)] + " ");
            weaponName.Append(Gamerules.weaponTypeWeaponName[randomWeaponType]);
            wpn = new Weapon(
                name: weaponName.ToString(),
                slot: EquipmentSlot.SLOT_WEAPON,
                levelRequirement: h.Level,
                category: randomWeaponType,
                baseDamage: h.Level + rand.Next(-h.Level, h.Level),
                attackSpeed: rand.Next(1, 5) + (float)rand.NextDouble());
            return wpn;
        }

        /// <summary>
        /// Generates a random piece of armor based on a hero
        /// </summary>
        /// <param name="h">hero to base armor on</param>
        /// <returns>random piece of armor</returns>
        public static Armor GenerateRandomArmor(Hero h)
        {
            Armor armr;
            Array matArr = Enum.GetValues(typeof(Material));
            List<EquipmentSlot> slotList = Enum.GetValues(typeof(EquipmentSlot)).Cast<EquipmentSlot>().ToList();
            slotList.Remove(EquipmentSlot.SLOT_WEAPON);
            Material randomMaterial = (Material)matArr.GetValue(rand.Next(matArr.Length));
            EquipmentSlot randomSlot = slotList[rand.Next(slotList.Count)];

            StringBuilder armorName = new StringBuilder();
            armorName.Append(Gamerules.AdjectivePrefixes[rand.Next(Gamerules.AdjectivePrefixes.Count)] + " ");
            armorName.Append(Gamerules.MaterialToString[randomMaterial] + " ");
            armorName.Append(Gamerules.EquipmentSlotArmorName[randomSlot]);
            armr = new Armor(
                name: armorName.ToString(), 
                slot: randomSlot, 
                levelRequirement: h.Level, 
                material: randomMaterial, 
                attributes: new PrimaryAttributes(rand.Next(0, 10), rand.Next(0, 10), rand.Next(0, 10), rand.Next(0, 10)));
            return armr;
        }
    }
}
