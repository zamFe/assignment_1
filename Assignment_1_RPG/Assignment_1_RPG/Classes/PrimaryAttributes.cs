﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1_RPG.Classes
{
    public class PrimaryAttributes
    {
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }
        public int Vitality { get; set; }

        public PrimaryAttributes() { }
        public PrimaryAttributes(int strength, int dexterity, int intelligence, int vitality)
        {
            this.Strength = strength;
            this.Dexterity = dexterity;
            this.Intelligence = intelligence;
            this.Vitality = vitality;
        }

        public static PrimaryAttributes operator+ (PrimaryAttributes a, PrimaryAttributes b)
        {
            PrimaryAttributes c = new PrimaryAttributes();
            c.Strength = a.Strength + b.Strength;
            c.Dexterity = a.Dexterity + b.Dexterity;
            c.Intelligence = a.Intelligence + b.Intelligence;
            c.Vitality = a.Vitality + b.Vitality;
            return c;
        }

        public override bool Equals(object obj)
        {
            return obj is PrimaryAttributes attributes &&
                   Strength == attributes.Strength &&
                   Dexterity == attributes.Dexterity &&
                   Intelligence == attributes.Intelligence &&
                   Vitality == attributes.Vitality;
        }
        public override int GetHashCode()
        {
            return HashCode.Combine(Strength, Dexterity, Intelligence, Vitality);
        }
        public override string ToString()
        {
            return $"str:{Strength} dex:{Dexterity} int:{Intelligence} vit:{Vitality}";
        }
    }
}
