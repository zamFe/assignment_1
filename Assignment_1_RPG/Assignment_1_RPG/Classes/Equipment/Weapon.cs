﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1_RPG.Classes
{
    public class Weapon : Equipment
    {
        public string name { get; set; }
        public EquipmentSlot slot { get; set; }
        public int levelRequirement { get; set; }
        public WeaponType category { get; set; }
        public int baseDamage { get; set; }
        public float attackSpeed { get; set; }

        public Weapon() { }

        public Weapon(string name, EquipmentSlot slot, int levelRequirement, WeaponType category, int baseDamage, float attackSpeed) 
        {
            this.name = name;
            this.slot = slot;
            this.levelRequirement = levelRequirement;
            this.category = category;
            this.baseDamage = baseDamage;
            this.attackSpeed = attackSpeed;
        }

        /// <summary>
        /// Returns the weapon DPS (baseDamage * attackSpeed)
        /// </summary>
        /// <returns>the product of baseDamage and attackSpeed</returns>
        public float GetDPS()
        {
            return baseDamage * attackSpeed;
        }

        public override string ToString()
        {
            return $"{name} (dmg:{baseDamage} spd:{attackSpeed})";
        }
    }
}
