﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1_RPG.Classes
{
    public interface Equipment
    {
        public string name { get; set; }
        public EquipmentSlot slot { get; set; }
        public int levelRequirement { get; set; }
    }
}
