﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1_RPG.Classes
{
    public class Armor : Equipment
    {
        public string name { get; set; }
        public EquipmentSlot slot { get; set; }
        public int levelRequirement { get; set; }

        public Material material { get; set; }
        public PrimaryAttributes attributes { get; set; }

        public Armor() { }

        public Armor(string name, EquipmentSlot slot, int levelRequirement, Material material, PrimaryAttributes attributes)
        {
            this.name = name;
            this.slot = slot;
            this.levelRequirement = levelRequirement;
            this.material = material;
            this.attributes = attributes;
        }
    }
}
