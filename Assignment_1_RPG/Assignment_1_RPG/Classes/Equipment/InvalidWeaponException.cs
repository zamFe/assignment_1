﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1_RPG.Classes
{
    [Serializable]
    public class InvalidWeaponException : Exception
    {
        public InvalidWeaponException() : base() { }
        public InvalidWeaponException(string name, int level) 
            : base($"{name} requires level {level}") { }
        public InvalidWeaponException(WeaponType weaponType)
            : base($"cannot equip weapon of type {weaponType}") { }
    }
}
