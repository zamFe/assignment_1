﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1_RPG.Classes
{
    [Serializable]
    public class InvalidArmorException : Exception
    {
        public InvalidArmorException() : base() { }
        public InvalidArmorException(string armorName, int level)
            : base($"{armorName} requires level {level}") { }
        public InvalidArmorException(Material material)
            : base($"cannot equip armor made out of {material}") { }
    }
}
