﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1_RPG.Classes
{
    public class SecondaryAttributes
    {
        public PrimaryAttributes totalAttributes = new();

        public SecondaryAttributes() { }
        public SecondaryAttributes(PrimaryAttributes totalAttributes)
        {
            this.totalAttributes = totalAttributes;
        }

        public void UpdateTotalAttributes(PrimaryAttributes attributes)
        {
            this.totalAttributes = attributes;
        }

        /// <summary>
        /// Calculates Health attribute
        /// </summary>
        /// <returns>health as int</returns>
        public int Health()
        {
            return totalAttributes.Vitality * 10;
        }

        /// <summary>
        /// Calculates Armor attribute
        /// </summary>
        /// <returns>armor as int</returns>
        public int ArmorRating()
        {
            return totalAttributes.Strength + totalAttributes.Dexterity;
        }

        /// <summary>
        /// Calculates Elemental resistance attribute
        /// </summary>
        /// <returns>elemental resistance as int</returns>
        public int ElementalResistance()
        {
            return totalAttributes.Intelligence;
        }
    }
}
