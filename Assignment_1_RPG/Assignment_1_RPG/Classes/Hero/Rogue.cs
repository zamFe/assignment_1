﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1_RPG.Classes
{
    public class Rogue : Hero
    {
        public Rogue(string name) : base(name)
        {
            this.basePrimaryAttributes = new PrimaryAttributes(2, 6, 1, 8);
            secondaryAttributes.UpdateTotalAttributes(basePrimaryAttributes);
        }

        /// <summary>
        /// Calls Equip in Base Class
        /// </summary>
        /// <param name="item">Item to equip</param>
        /// <returns>confirmation string</returns>
        public string Equip(Equipment item)
        {
            return EquipItem(item, "Rogue");
        }

        /// <summary>
        /// Calls LevelUp in base class, 
        /// and increases base attributes
        /// </summary>
        /// <param name="amount">level amount</param>
        public override void LevelUp(int amount)
        {

            base.LevelUp(amount);
            basePrimaryAttributes += Gamerules.HeroLevelGains["Rogue"];
            secondaryAttributes.UpdateTotalAttributes(basePrimaryAttributes);
        }
    }
}
