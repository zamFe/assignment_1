﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1_RPG.Classes
{
    public abstract class Hero
    {
        public string Name { get; }
        public int Level { get; set; } = 1;
        public int Gold { get; set; } = 10;

        public PrimaryAttributes basePrimaryAttributes { get; set; }
        public PrimaryAttributes totalPrimaryAttributes { get; set; }
        public SecondaryAttributes secondaryAttributes { get; set; } = new();
        public Dictionary<EquipmentSlot, Equipment> equipment { get; set; } = new Dictionary<EquipmentSlot, Equipment>();

        public Hero(string name)
        {
            this.Name = name;
            this.Level = 1;
            this.secondaryAttributes.totalAttributes = basePrimaryAttributes;
            this.totalPrimaryAttributes = new(0, 0, 0, 0);
        }

        public Hero(string name, int level)
        {
            this.Name = name;
            this.Level = level;
            this.secondaryAttributes.totalAttributes = basePrimaryAttributes;
            this.totalPrimaryAttributes = new(0, 0, 0, 0);
        }

        /// <summary>
        /// Updates the total primary attributes 
        /// by setting them to base value and
        /// adding on any equipped armor piece
        /// </summary>
        public void UpdateTotalAttributes()
        {
            totalPrimaryAttributes.Strength = basePrimaryAttributes.Strength;
            totalPrimaryAttributes.Vitality = basePrimaryAttributes.Vitality;
            totalPrimaryAttributes.Intelligence = basePrimaryAttributes.Intelligence;
            totalPrimaryAttributes.Dexterity = basePrimaryAttributes.Dexterity;
            totalPrimaryAttributes += basePrimaryAttributes;
            if(equipment.ContainsKey(EquipmentSlot.SLOT_HEAD))
            {
                Armor head = (Armor)equipment[EquipmentSlot.SLOT_HEAD];
                totalPrimaryAttributes += head.attributes;
            }
            if (equipment.ContainsKey(EquipmentSlot.SLOT_BODY))
            {
                Armor body = (Armor)equipment[EquipmentSlot.SLOT_BODY];
                totalPrimaryAttributes += body.attributes;
            }
            if (equipment.ContainsKey(EquipmentSlot.SLOT_LEGS))
            {
                Armor legs= (Armor)equipment[EquipmentSlot.SLOT_LEGS];
                totalPrimaryAttributes += legs.attributes;
            }
        }

        /// <summary>
        /// Increments one level to the characters
        /// Throws ArgumentException on 0 og negative amount
        /// </summary>
        /// <param name="amount">amount to level up</param>
        public virtual void LevelUp(int amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("level amount cannot be 0 or negative");
            }
            else
            {
                this.Level += amount;
                Console.WriteLine($"{this.Name} just leveled up to {this.Level}!");
            }
        }

        /// <summary>
        /// Equips item (armor or weapon) based on hero type.
        /// Throws InvalidWeaponException on invalid levelRequirement or weapon category
        /// Throws InvalidArmorException on invalid levelRequirement or armor material
        /// Throws InvalidCastException on failed cast
        /// </summary>
        /// <param name="item"></param>
        /// <param name="heroType"></param>
        /// <returns>confirmation string</returns>
        public string EquipItem(Equipment item, string heroType)
        {
            if(item.slot == EquipmentSlot.SLOT_WEAPON)
            {
                if (item.levelRequirement > this.Level)
                {
                    throw new InvalidWeaponException(item.name, item.levelRequirement);
                } 
                else
                {
                    try
                    {
                        Weapon wpn = (Weapon)item;
                        if (!Gamerules.HeroSupportedWeapons[heroType].Contains(wpn.category))
                            throw new InvalidWeaponException(wpn.category);
                        equipment[item.slot] = item;
                        return "New weapon equipped!";
                    }
                    catch (InvalidCastException e)
                    {
                        return e.Message;
                    }
                }
            }
            else
            {
                if (item.levelRequirement > this.Level)
                {
                    throw new InvalidArmorException(item.name, item.levelRequirement);
                } 
                else
                {
                    try
                    {
                        Armor armor = (Armor)item;
                        if (!Gamerules.HeroSupportedMaterials[heroType].Contains(armor.material))
                            throw new InvalidArmorException(armor.material);
                        equipment[item.slot] = item;
                        UpdateTotalAttributes();
                        return "New armor equipped!";
                    }
                    catch (InvalidCastException e)
                    {
                        return e.Message;
                    }
                }
            }
        }

        /// <summary>
        /// Calculates DPS for hero based off weapon DPS and attributes
        /// Throws InvalidCastException on failed cast
        /// Throws KeyNotFoundException on key not found in dictionary
        /// Throws NullReferenceException if no weapon in slot
        /// </summary>
        /// <returns>Hero DPS as float</returns>
        public virtual float CalculateDPS()
        {
            float weaponDPS = 1;
            try
            {
                Weapon wpn = (Weapon)equipment[EquipmentSlot.SLOT_WEAPON];
                weaponDPS = wpn.GetDPS();
            }
            catch (InvalidCastException e)
            {
                equipment[EquipmentSlot.SLOT_WEAPON] = new Weapon();
            }
            catch (Exception e)
            {
                if(e is KeyNotFoundException || e is NullReferenceException)
                weaponDPS = 1;
            }
            int primaryAttribute = 0;
            UpdateTotalAttributes();
            switch (this.GetType().Name)
            {
                case "Mage":
                    primaryAttribute = totalPrimaryAttributes.Intelligence;
                    break;
                case "Ranger":
                case "Rogue":
                    primaryAttribute = totalPrimaryAttributes.Dexterity;
                    break;
                case "Warrior":
                    primaryAttribute = totalPrimaryAttributes.Strength;
                    break;
            }

            return weaponDPS * (1 + primaryAttribute / 100);
        }

        /// <summary>
        /// Returns armor-stats as string
        /// </summary>
        /// <returns>armor-stats as string</returns>
        public string ArmorStats()
        {
            PrimaryAttributes output = new PrimaryAttributes(0, 0, 0, 0);
            if (equipment.ContainsKey(EquipmentSlot.SLOT_HEAD))
            {
                Armor head = (Armor)equipment[EquipmentSlot.SLOT_HEAD];
                output += head.attributes;
            }
            if (equipment.ContainsKey(EquipmentSlot.SLOT_BODY))
            {
                Armor body = (Armor)equipment[EquipmentSlot.SLOT_BODY];
                output += body.attributes;
            }
            if (equipment.ContainsKey(EquipmentSlot.SLOT_LEGS))
            {
                Armor legs = (Armor)equipment[EquipmentSlot.SLOT_LEGS];
                output += legs.attributes;
            }
            return output.ToString();
        }

        /// <summary>
        /// Returns string of information about the hero
        /// </summary>
        /// <returns>stats string</returns>
        public virtual string Stats()
        {
            StringBuilder s = new StringBuilder("==========");
            s.AppendLine();
            s.AppendLine("Gold:" + this.Gold);
            s.AppendLine();
            s.AppendLine("Name: " + this.Name);
            s.AppendLine("Class: " + this.GetType().Name);
            s.AppendLine("Level: " + this.Level);
            s.AppendLine("Strength: " + this.totalPrimaryAttributes.Strength);
            s.AppendLine("Dexterity: " + this.totalPrimaryAttributes.Dexterity);
            s.AppendLine("Intelligence: " + this.totalPrimaryAttributes.Intelligence);
            s.AppendLine("Health: " + this.secondaryAttributes.Health());
            s.AppendLine("Armor Rating: " + this.secondaryAttributes.ArmorRating());
            s.AppendLine("Elemental Resistance: " + this.secondaryAttributes.ElementalResistance());
            if (equipment.ContainsKey(EquipmentSlot.SLOT_WEAPON))
            {
                Weapon w = (Weapon)equipment[EquipmentSlot.SLOT_WEAPON];
                s.AppendLine("Weapon: " + w.ToString());
            } 
            else
            {
                s.AppendLine("Weapon: bare fists");
            }
            s.AppendLine("Armor: " + secondaryAttributes.ArmorRating());
            s.AppendLine("DPS: " + CalculateDPS());
            s.AppendLine("==========");
            return s.ToString();
        }
    }
}
