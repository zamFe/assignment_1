﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1_RPG.Classes
{
    public class Enemy : Hero
    {

        public Enemy(string name, int level, PrimaryAttributes attributes) : base(name, level)
        {
            this.basePrimaryAttributes = attributes;
            secondaryAttributes.UpdateTotalAttributes(basePrimaryAttributes);
        }

        /// <summary>
        /// Calls Equip in Base Class
        /// </summary>
        /// <param name="item">Item to equip</param>
        /// <returns>confirmation string</returns>
        public string Equip(Equipment item)
        {
            return base.EquipItem(item, "Mage");
        }
    }
}

