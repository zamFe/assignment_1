﻿using System;
using System.Threading;

namespace Assignment_1_RPG.Classes
{
    class Program
    {

        public static Random rand = new Random();

        static void Main(string[] args)
        {

            /* INTRO */

            Console.ForegroundColor = ConsoleColor.Cyan;
            //Console.WriteLine("=== !Console RPG! ===");
            Console.WriteLine(@"          _____ ____  _   _  _____  ____  _      ______      _____  _____   _____          ");
            Wait(500);
            Console.WriteLine(@"  ______ / ____/ __ \| \ | |/ ____|/ __ \| |    |  ____|    |  __ \|  __ \ / ____|  ______ ");
            Wait(500);
            Console.WriteLine(@" |______| |   | |  | |  \| | (___ | |  | | |    | |__ ______| |__) | |__) | |  __  |______|");
            Wait(500);
            Console.WriteLine(@"  ______| |   | |  | | . ` |\___ \| |  | | |    |  __|______|  _  /|  ___/| | |_ |  ______ ");
            Wait(500);
            Console.WriteLine(@" |______| |___| |__| | |\  |____) | |__| | |____| |____     | | \ \| |    | |__| | |______|");
            Wait(500);
            Console.WriteLine(@"         \_____\____/|_| \_|_____/ \____/|______|______|    |_|  \_\_|     \_____|         ");


            Spacing(3);
            Wait(1000);

            /* NAME SELECTION */

            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(".");
            Wait(300);
            Console.WriteLine("..");
            Wait(300);
            Console.WriteLine("...");
            Wait(600);
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("[???]:");
            Wait(600);
            Console.WriteLine("Adventurer... what shall i call you?");
            Console.ForegroundColor = ConsoleColor.White;
            string nameInput = Console.ReadLine();
            Console.WriteLine();
            Wait(600);
            /* HERO CLASS SELECTION */
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("[???]:");
            Wait(600);
            Console.WriteLine($"Hmm... {nameInput}. What is your calling in this life?");
            Wait(600);
            Console.ForegroundColor = ConsoleColor.White;
            Spacing(1);
            Console.WriteLine("1. Mage    - you have studied the ancient books and mastered the staff.               Damage increased by intelligence");
            Console.WriteLine("2. Ranger  - growing up in the woods, you've become an expert with the bow.           Damage increased by Dexterity");
            Console.WriteLine("3. Rogue   - as a thief, the blade has been by your side during difficult times.      Damage increased by Dexterity");
            Console.WriteLine("4. Warrior - trained for combat your entire life, you wield heavy weapons with grace. Damage increased by Strength");

            Hero hero;

            int HeroInput;
            Int32.TryParse(Console.ReadLine(), out HeroInput);
            switch (HeroInput)
            {
                case 1:
                    hero = new Mage(nameInput);
                    break;
                case 2:
                    hero = new Ranger(nameInput);
                    break;
                case 3:
                    hero = new Rogue(nameInput);
                    break;
                case 4:
                    hero = new Warrior(nameInput);
                    break;
                default:
                    hero = new Warrior(nameInput);
                    break;
            }

            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine(hero.Stats());
            Console.ForegroundColor = ConsoleColor.White;

            /* WEAPON GIFT */

            Wait(3000);
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("[???]:");
            Wait(1000);
            Console.WriteLine($"Very well {hero.Name} the {hero.GetType().Name}.");
            Wait(3000);
            Console.WriteLine($"bla bla bla big quest blablblrbl");
            Wait(1000);
            Console.WriteLine($"something about saving someone");
            Wait(1000);
            Console.WriteLine($"mmmm dragons and stuff");
            Wait(1000);
            Console.WriteLine($"...");
            Wait(1000);
            Console.WriteLine("Here, take this to protect yourself:");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("<press ENTER to take Weapon>");
            Spacing(1);

            Weapon starterWeapon = Gamerules.StarterWeapons[hero.GetType().Name];
            hero.EquipItem(starterWeapon, hero.GetType().Name);
            Console.ForegroundColor = ConsoleColor.Green;
            Spacing(1);
            Console.WriteLine($"You recieved the {starterWeapon.ToString()}!");
            Spacing(1);
            Wait(3000);

            GameLoop(hero);
        }

        /// <summary>
        /// The game-loop itself. Connection all the functionality with a main-menu
        /// </summary>
        /// <param name="hero">hero</param>
        public static void GameLoop(Hero hero)
        {
            while (hero.secondaryAttributes.Health() > 0)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("==========");
                Console.WriteLine("Action Menu:");
                Console.WriteLine("1. Explore to gain experience");
                Console.WriteLine("2. Training fight");
                Console.WriteLine("3. Buy Weapon");
                Console.WriteLine("4. Check Stats");
                Console.WriteLine("==========");

                int input = 0;
                Int32.TryParse(Console.ReadLine(), out input);
                switch(input)
                {
                    case 1:
                        Explore(hero);
                        break;
                    case 2:
                        Combat(hero);
                        break;
                    case 4:
                        Console.ForegroundColor = ConsoleColor.DarkYellow;
                        Console.WriteLine(hero.Stats());
                        break;
                }
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.WriteLine("<press ENTER to continue your adventure>");
                Spacing(1);
                //TODO: add more content
            }
        }

        public static void Wait(int amount)
        {
            Thread.Sleep(amount);
        }

        public static void Spacing(int amount)
        {
            for(int i = 0; i < amount; i++)
            {
                Console.WriteLine();
            }
        }

        public static void Spacing(int amount, int delay)
        {
            for (int i = 0; i < amount; i++)
            {
                Console.WriteLine();
                Wait(delay);
            }
        }

        /// <summary>
        /// Initiates combat with a random enemy.
        /// </summary>
        /// <param name="h">hero</param>
        public static void Combat(Hero h)
        {
            int HeroHP = h.secondaryAttributes.Health();

            Enemy enemy = new Enemy(
                name: Gamerules.EnemyNames[rand.Next(Gamerules.EnemyNames.Count)],
                level: rand.Next(h.Level - 1, h.Level + 2),
                attributes: h.totalPrimaryAttributes);

            int EnemyHP = enemy.secondaryAttributes.Health();

            Spacing(10, 200);


            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine($"{enemy.Name} appeared from the shadows!");

            while(HeroHP > 0 && EnemyHP > 0)
            {
                //display stats
                Spacing(1);
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine($"[Lvl: {h.Level}]{h.Name}: {HeroHP}HP");
                Console.WriteLine($"[Lvl: {enemy.Level}]{enemy.Name}: {EnemyHP}HP");
                Spacing(2);
                //ask for action
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Choose your action: ");
                Console.WriteLine("1. Attack");
                Console.WriteLine("2. Heal");
                Console.WriteLine("3. Flee (-5 Gold)");
                int input;
                Int32.TryParse(Console.ReadLine(), out input);
                //calculate and report 
                switch (input)
                {
                    case 1:
                        Weapon wpn = (Weapon)h.equipment[EquipmentSlot.SLOT_WEAPON];
                        int dmg = rand.Next((int)h.CalculateDPS(), (int)h.CalculateDPS() * 3) + h.Level;
                        Wait(500);
                        Console.ForegroundColor = ConsoleColor.Blue;
                        Console.WriteLine($"Your struck the {enemy.Name} with your {wpn.name}");
                        Wait(1000);
                        Console.ForegroundColor = ConsoleColor.Gray;
                        Console.WriteLine($"You dealt {dmg} damage!");
                        EnemyHP -= dmg;
                        if(EnemyHP <= 0)
                        {
                            Spacing(1, 1000);
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine($"{enemy.Name} has been slain!");
                            Wait(1000);
                            int reward = rand.Next(1, enemy.Level);
                            h.Gold += reward;
                            Console.WriteLine($"You looted {reward} gold!");
                            Wait(1000);
                            return;
                        }
                        break;
                    case 2:
                        Wait(500);
                        Console.ForegroundColor = ConsoleColor.Blue;
                        Console.WriteLine("You use your vitality to heal your injuries");
                        int healing = rand.Next(h.totalPrimaryAttributes.Vitality);
                        Wait(1000);
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine($"You regained {healing} HP");
                        HeroHP += healing;
                        if(HeroHP > h.secondaryAttributes.Health())
                        {
                            HeroHP = h.secondaryAttributes.Health();
                        }
                        break;
                    case 3:
                        if(h.Gold >= 5)
                        {
                            h.Gold -= 5;
                            Wait(500);
                            Console.ForegroundColor = ConsoleColor.Blue;
                            Console.WriteLine($"You fled from the grasp of the {enemy.Name}, but you lost 5 gold in the process...");
                            return;
                        }
                        break;
                }
                //excecute enemy action
                Wait(1000);
                int enemyDPS;
                if(enemy.Level >= h.Level)
                {
                    enemyDPS = rand.Next((int)h.CalculateDPS(), (int)h.CalculateDPS() * 3) + enemy.Level;
                } else
                {
                    enemyDPS = rand.Next(((int)h.CalculateDPS()) * 2) + enemy.Level;
                }
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"{enemy.Name} plunges at you, dealing {enemyDPS} damage!");
                HeroHP -= enemyDPS;
                if (HeroHP <= 0)
                {
                    Console.WriteLine();
                    Wait(1000);
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine($"You have been defeated by the {enemy.Name}");
                    Wait(1000);
                    int loss = rand.Next(0, h.Gold/2);
                    h.Gold -= loss;
                    Console.WriteLine($"You woke up with {loss} gold missing from your pockets");
                    Wait(1000);
                    return;
                }
                Wait(1000);
            }
        }

        /// <summary>
        /// Sends the hero on an adventure, resulting in a randomly chosen event!
        /// </summary>
        /// <param name="h">hero</param>
        public static void Explore(Hero h)
        {
            switch(rand.Next(1, 3)) {
                case 1:
                    //Discovery
                    Console.ForegroundColor = ConsoleColor.Green;
                    Wait(1000);
                    Console.WriteLine("You've discovered a new location:");
                    Wait(1000);
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.WriteLine(Gamerules.Locations[rand.Next(0, Gamerules.Locations.Count)]);
                    Wait(1000);
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("You gained more experience and leveled up!");
                    h.LevelUp(1);
                    Wait(1000);
                    Spacing(1);
                    break;
                case 2:
                    //Chest
                    Console.ForegroundColor = ConsoleColor.Green;
                    Wait(1000);
                    Console.WriteLine("You found a chest!");
                    Console.ForegroundColor = ConsoleColor.Gray;
                    Console.WriteLine("<press ENTER to open chest>");
                    Console.ReadLine();
                    Wait(400);
                    Console.WriteLine("...");
                    Wait(400);
                    Console.WriteLine("...");
                    Wait(400);
                    Console.WriteLine("...");
                    Wait(1000);

                    Equipment loot;
                    if (rand.Next(0, 2) == 0)
                    {
                        loot = Gamerules.GenerateRandomWeapon(h);
                    }
                    else
                    {
                        loot = Gamerules.GenerateRandomArmor(h);
                    }
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.WriteLine($"The chest contained a {loot.name}! [Lvl: {loot.levelRequirement}]");
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("Do you wish to equip it?");
                    Console.ForegroundColor = ConsoleColor.Gray;
                    Console.WriteLine("1. Yes!");
                    Console.WriteLine("2. No!");
                    int input;
                    Int32.TryParse(Console.ReadLine(), out input);
                    Wait(1000);
                    if(input == 1)
                    {
                        try
                        {
                            Console.WriteLine(h.EquipItem(loot, h.GetType().Name));
                        }
                        catch (InvalidWeaponException)
                        {
                            Console.ForegroundColor = ConsoleColor.DarkGray;
                            Console.WriteLine("...");
                            Wait(1000);
                            Console.WriteLine("uhhmm...");
                            Wait(1000);
                            Weapon wpn = (Weapon)loot;
                            Console.WriteLine($"seems like you, being a {h.GetType().Name}, have no idea on how to wield the {Gamerules.weaponTypeWeaponName[wpn.category]}");
                            Wait(2000);
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("soooo you threw it in a bush ¯\\_(ツ)_/¯");
                            Wait(2000);
                        }
                        catch (InvalidArmorException)
                        {
                            Console.ForegroundColor = ConsoleColor.DarkGray;
                            Console.WriteLine("...");
                            Wait(1000);
                            Console.WriteLine("uhhmm...");
                            Wait(1000);
                            Armor armr = (Armor)loot;
                            Console.WriteLine($"seems like you, being a {h.GetType().Name}, have no idea on how to wear armor made out of {Gamerules.MaterialToString[armr.material]}");
                            Wait(2000);
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("soooo you threw it in a bush ¯\\_(ツ)_/¯");
                            Wait(2000);
                        }
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.Gray;
                        Console.WriteLine($"you left the {loot.name}");
                    }
                    
                    break;
            }
        }
    }

}
