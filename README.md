# Console RPG!

Fight your way through the command prompt as the Hero of a text-based quest for glory! 

![start screen](https://gitlab.com/zamFe/assignment_1/-/raw/49cdce859594b566d6dda271602c1f274c0d28a5/Assignment_1_RPG/Assignment_1_RPG/Classes/Screenshots/start_screen.png)

## Be the Hero
Maybe you're an intelligent mage, or a bow-skilled ranger. 
Sneak your way to victory as a rogue,
or use brute force to smash your enemies as a warrior!

![combat](https://gitlab.com/zamFe/assignment_1/-/raw/49cdce859594b566d6dda271602c1f274c0d28a5/Assignment_1_RPG/Assignment_1_RPG/Classes/Screenshots/combat.png)

## Explore forgotten lands
Travel across foreign lands to gain experience. 
Maybe you'll even stumble over a treasure or two!

![explore](https://gitlab.com/zamFe/assignment_1/-/raw/49cdce859594b566d6dda271602c1f274c0d28a5/Assignment_1_RPG/Assignment_1_RPG/Classes/Screenshots/looting.png)

## Level Up
As you explore and fight through the waves of text, your character will grow stronger, unlocking powerful equipment, but also stronger enemies

![stats](https://gitlab.com/zamFe/assignment_1/-/raw/49cdce859594b566d6dda271602c1f274c0d28a5/Assignment_1_RPG/Assignment_1_RPG/Classes/Screenshots/stats.png)
